#include "Definitions.h"

#ifdef CONSTANTS
#include <stdio.h>
//#include <string.h>

// integer constant
#define INTEGER 1234

// long constants
#define LONG_l 123456789l
#define LONG_L 123456789L

// float constants
#define FLOAT_f 123.4f
#define FLOAT_F 123.4F

// floating-point constants
#define DOUBLE 123.4
#define DOUBLE_e_0 1234e-1
#define DOUBLE_e_1 123.4e-2
#define DOUBLE_E 1234.5E-3
#define DOUBLE_LONG_l 123456789.1l
#define DOUBLE_LONG_L 123456789.1L 

// octal constants
#define OCTAL_INTEGER 01234
#define OCTAL_LONG_l 0123456777l
#define OCTAL_LONG_L 0123456777L
#define OCTAL_UNSIGNED_u 01234u
#define OCTAL_UNSIGNED_U 01234U
#define OCTAL_UNSIGNED_LONG_ul 024677ul
#define OCTAL_UNSIGNED_LONG_UL 013577UL
#define OCTAL_UNSIGNED_LONG_uL 0123456777Ul
#define OCTAL_UNSIGNED_LONG_Ul 012345677777uL

// hexadecimal constants
#define HEXADECIMAL_0x 0x1a2b
#define HEXADECIMAL_0X 0X1A2B
#define HEXADECIMAL_0x_LONG_l 0x1a2bl
#define HEXADECIMAL_0x_LONG_L 0x3c4dL
#define HEXADECIMAL_0X_LONG_l 0X1A2Bl
#define HEXADECIMAL_0X_LONG_L 0X3C4DL
#define HEXADECIMAL_0x_UNSIGNED_u 0x1a2bu
#define HEXADECIMAL_0x_UNSIGNED_U 0x3c4dU
#define HEXADECIMAL_0X_UNSIGNED_u 0X1A2Bu
#define HEXADECIMAL_0X_UNSIGNED_U 0X3C4DU
#define HEXADECIMAL_0x_UNSIGNED_LONG_ul 0x1a2b3c4dul
#define HEXADECIMAL_0x_UNSIGNED_LONG_UL 0x5e6f7a8bUL
#define HEXADECIMAL_0x_UNSIGNED_LONG_uL 0x1a2b3c4duL
#define HEXADECIMAL_0x_UNSIGNED_LONG_Ul 0x5e6f7a8bUl
#define HEXADECIMAL_0X_UNSIGNED_LONG_ul 0X1A2B3C4DuL
#define HEXADECIMAL_0X_UNSIGNED_LONG_UL 0X5E6F7A8BUL
#define HEXADECIMAL_0X_UNSIGNED_LONG_uL 0X1A2B3C4DuL
#define HEXADECIMAL_0X_UNSIGNED_LONG_Ul 0X5E6F7A8BUl

// character constants
#define CHAR 'a'
#define CHAR_ESCAPE '\0'				// Null character (value is 0)
#define CHAR_ESCAPE_OCTAL '\013'		// ASCII vertical tab
#define CHAR_ESCAPE_HEXADECIMAL '\xb'	// ASCII vertical tab

// string constan
#define STRING "This is a string constant"
#define STRING_SEPARATED "Hello, " "world!"

enum boolean { NO, YES };
enum escapes { BELL = '\a', BACKSPACE = '\b', TAB = '\t', NEWLINE = '\n', VTAB = '\v', RETURN = '\r' };
enum months { JAN = 1, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC }; /* FEB is 2, MAR is 3, etc. */

/* strlen: return length of s */
int strlen(char s[])
{
	int i;

	i = 0;
	while (s[i] != '\0')
		i++;
	return i;
}

int main()
{
	printf("strlen: %d\n", strlen(STRING_SEPARATED));

	return 0;
}

#endif // CONSTANTS