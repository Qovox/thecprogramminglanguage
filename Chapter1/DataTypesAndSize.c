#include "Definitions.h"

#ifdef DATA_TYPES_AND_SIZES
#include <stdio.h>
#include <float.h> // Contains symbolic constants for types' sizes
#include <limits.h> // Contains symbolic constants for types' sizes

/*
 * Remarks:
 * By default any type is signed, the keyword signed can be ommitted.
 * The keyword int can be ommitted when using short and long integer types.
 * The default type of a variable is int when there is no primitive type 
 * specified but only modifiers.
 * There are 4 primitive types and 4 modifiers which are, respectively:
 * char, int, float, double, and signed, unsigned, short, long.
 * The comment after each variable is the printf format specifier.
 */

/********** Types **********/

// char
char c;							// %c
signed char signed_char;		// %c (%hhi for numerical output)
unsigned char unsigned_char;	// %c (%hhu for numerical output)

// int
int i;							// %i or %d
signed int signed_int;			// %i or %d
unsigned int unsigned_int;		// %u

// float
float f; // %f or %F for digital notation. %g, %G, %e, %E, %a or %A for scientific notation

// double
double d;					// %lf, %lF, %lg, %lG, %le, %lE, %la or %lA. The length modifier l is not optional
long double long_double;	// %Lf, %LF, %Lg, %LG, %Le, %LE, %La or %LA

/*******************************/

/********** Modifiers **********/

// signed modifier
signed signed_modifier; // %i or %d

// unsigned modifier
unsigned unsigned_modifier; // %u

// short modifier
short s;								// %hi
short int short_int;					// %hi

signed short signed_short;				// %hi
signed short int signed_short_int;		// %hi

unsigned short unsigned_short;			// %hu
unsigned short int unsigned_short_int;	// %hu

// long modifier
long l;											// %li
long int long_int;								// %li

signed long signed_long;						// %li
signed long int signed_long_int;				// %li

unsigned long unsigned_long_int;				// %lu
unsigned long int unsigned_long_int;			// %lu

long long ll;									// %lli
long long int long_long_int;					// %lli

signed long long signed_long_long;				// %lli
signed long long int signed_long_long_int;		// %lli

unsigned long long unsigned_long_long_int;		// %llu
unsigned long long int unsigned_long_long_int;	// %llu

/*******************************/

int main()
{
	signed_char = 200;
	unsigned_char = -12;

	printf("signed char(200)=%d; unsigned char(-12)=%d;\n", signed_char, unsigned_char);

	return 0;
}

#endif // DATA_TYPES_AND_SIZES